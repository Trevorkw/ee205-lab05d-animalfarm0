/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    deletCats.c
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "addCats.h"
#include "updateCats.h"
#include "deleteCats.h"


void deleteAllCats() {
   printf("All cats deleted\n");
   for(int i = 0; i<= MAX_CATS; i++){
   	
      strcpy(name[i], "x");
      isFixed[i] = false;
      weight[i] = 0;
      cat_gender[i] = UNKNOWN_GENDER;
      cat_breed[i] = UNKNOWN_BREED;
   }
}
