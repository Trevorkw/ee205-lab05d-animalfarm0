/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    addCats.c
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "addCats.h"

int valid(char cat_name[], float cat_weight){
   if (catCount >= MAX_CATS){ //database cannot be full
      return 1;  //1 for error 
   }
   if(strlen(cat_name) == 0){ //cat name cannot be empty
      return 1;
   }
   if(strlen(cat_name) > MAX_CAT_NAME){ //name of cat cannot be greter than 30 
      return 1;
   }
   if(cat_weight <= 0){  //weight cannot be 0 or less
      return 1;
   }
   for(int i=0; i<catCount; i++){       //checks database to see if names are unique
      if(strcmp(cat_name, name[i]) == 0){
         return 1;
      }
      return 0;
      }
   }
   int addCat(char addName[],enum gender addGender,enum breed addBreed,bool addIsFixed,float addWeight) {
      if(valid(addName, addWeight) != 0) {
         return 1;
   }
      strcpy(name[catCount], addName);
      isFixed[catCount] = addIsFixed;
      weight[catCount] = addWeight;
      cat_gender[catCount] = addGender;
      cat_breed[catCount] = addBreed;
      catCount++;
      return catCount;
}
