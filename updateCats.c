/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    updateCats.c
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/

#include <stdio.h>
#include <string.h>

#include "updateCats.h"
#include "catDatabase.h"
#include "addCats.h"

int updateCatName(int index, char newName[]) {
   for(int i = 0; i < catCount; i++){
      if(strcmp(newName, name[i]) == 0){
         printf("Failure: name needs to be unique\n");  //test
         return 1;
      }
      if(strlen(newName) == 0) {
         return 1;
      }
   }
   strcpy(name[index], newName);
   return 0;
}

void fixCat(int index){
   isFixed[index] = true;
}

int updateCatWeight(int index, float newWeight){
   if(newWeight < 0) {
      printf("Failure: weight needs to be greater than 0\n"); ///test
      return 1;
   }
   weight[index] = newWeight;
   return 0;
}

