/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    reportCats.c
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/


#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"


int printCat(int catIndex){
   if(catIndex < 0 || catIndex > MAX_CATS) {
      printf("animalFarm0: Bad cat [%d]\n", catIndex);
      return 1;
   }
   printf("cat index = [%u] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", catIndex, name[catIndex], cat_gender[catIndex], cat_breed[catIndex], isFixed[catIndex], weight[catIndex]);
   return 0;
}

int printAllCats(){
   for(int i = 0; i < catCount; i++){
       printCat(i);
   }
   return 1;
}

int findCat(char catSearch[]) {
   for(int i = 0; i <= catCount; i++){
      if(strcmp(catSearch, name[i]) == 0){
         return i;
      }
   }
   printf("There is no cat with the name %s\n", catSearch);
   return -1;
}
