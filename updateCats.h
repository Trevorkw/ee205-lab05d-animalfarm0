/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    updateCats.h
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/

#pragma once
extern int updateCatName(int index, char newName[]);
extern void fixCat(int index);
extern int updateCatWeight(int index, float newWeight);
