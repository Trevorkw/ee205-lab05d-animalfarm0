/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    catDatabase.c
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "addCats.h"

//make the max size of the array to be only 30 cats 
int catCount;

enum gender cat_gender[MAX_CATS];
enum breed cat_breed[MAX_CATS];

char name[MAX_CATS][MAX_CAT_NAME];
bool isFixed[MAX_CATS];
float weight[MAX_CATS];
