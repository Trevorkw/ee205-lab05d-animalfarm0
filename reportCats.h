/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    reportCats.h
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/

#pragma once

extern int printCat(int catIndex);
extern int printAllCats();
extern int findCat(char scoutName[]);
