/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    catDatabase.h
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/

#include <stdio.h>
#include <stdbool.h> 
#define MAX_CATS 100
#define MAX_CAT_NAME 30
#pragma once

enum gender{MALE, FEMALE, UNKNOWN_GENDER};  //male, female, unknown
enum breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};     // unknown, main coon, manx, shorthair, persian, sphynx

extern enum gender cat_gender[MAX_CATS];                                //make array for gender
extern enum breed cat_breed[MAX_CATS];                                  //make array for breed

extern char name[][MAX_CAT_NAME]; //max size 30 character array for name
extern bool isFixed[];                                                      //bool is fixed
extern float weight[];                       //weight cannot be 0, use for and if loop?

extern int catCount;
 
