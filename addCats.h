/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
# 
# @file    addCats.h
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022 
*/

#pragma once

extern int valid(char cat_name[], float cat_weight);
extern int addCat(char addName[], enum gender addGender, enum breed addBreeed, bool addFixed, float addWeight);
