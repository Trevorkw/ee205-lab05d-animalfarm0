/*
#University of Hawaii, College of Engineering
#brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
#
#
# @file    main.c
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022
*/

#include <stdio.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "addCats.h"
#include "updateCats.h"
#include "deleteCats.h" 
#include "reportCats.h"

#define DEBUG 0 //how to turn off?

int main(){
      printf("Starting Animal Farm 0\n");
      addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
      #ifdef DEBUG
      printf("add cat to database\n");
      #endif

      addCat( "Milo", MALE, MANX, true, 7.0 ) ;
      #ifdef DEBUG
      printf("add cat to database\n");
      #endif

      addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
      #ifdef DEBUG
      printf("add cat to database\n");
      #endif

      addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
      #ifdef DEBUG
      printf("add cat to database\n");
      #endif

      addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
      #ifdef DEBUG
      printf("add cat to database\n");
      #endif

      addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
      #ifdef DEBUG
      printf("add cat to database\n");
      #endif
//only need one debug for adding cats?
      
      printAllCats();
      #ifdef DEBUG
      printf("print all cats\n");
      #endif

      int kali = findCat( "Kali" ) ;

      updateCatName( kali, "Chili" ) ; // this should fail
      //add an assert or debug?

      printCat( kali );
      #ifdef DEBUG
      printf("print specific cat\n");
      #endif

      updateCatName( kali, "Capulet" ) ;
      #ifdef DEBUG
      printf("update cat name\n");
      #endif

      updateCatWeight( kali, 9.9 ) ;
      #ifdef DEBUG
      printf("update cat weight\n");
      #endif
     
      fixCat( kali ) ;

      printCat( kali );
      #ifdef DEBUG
      printf("print specific cat\n");
      #endif

      printAllCats();
      #ifdef DEBUG
      printf("print all cats\n");
      #endif

      deleteAllCats();
      #ifdef DEBUG
      printf("remove all cats from database\n");
      #endif

      printAllCats();
      #ifdef DEBUG
      printf("print all cats\n");
      #endif

      addCat( "Arnold", MALE, MANX, false, 5.0);
      #ifdef DEBUG
      printf("add cat to database\n");
      #endif
      
      printAllCats();

      int Arnold = findCat("Arnold\n");

      updateCatWeight( Arnold, 0);
      #ifdef DEBUG
      printf("update cat weight to incorrect weight\n");
      #endif

      updateCatName(Arnold, "abcdefghijklmnopqrstuvwxyzabcdefg");
      #ifdef DEBUG
      printf("rename cat to incorrect name");
      #endif

      printCat( Arnold );
      #ifdef DEBUG
      printf("print specific cat\n");
      #endif

      printAllCats();
      #ifdef DEBUG
      printf("print all cats\n");
      #endif


      printf("Done with Animal Farm 0\n");
      return 0;
}

